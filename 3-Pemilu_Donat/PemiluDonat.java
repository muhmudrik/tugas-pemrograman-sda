/**
 * Data Structure and Alghoritm Project Assignment 3, about AVL Tree
 * 
 * Created by:
 * Muhammad Mudrik - 1806190935
 * 
 * This code cant be done without the help of
 * 1. GeeksForGeeks, especially Arnab Kundu
 * 2. Some advice from my friends about technical problem
 * 3. My Self for the hardwork all day and night
 * 4. Zafir for the idea of node with accumulative count
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

public class PemiluDonat {
    static Node root = null;
    static Donat donatA;
    static Donat donatB;
    static HashMap<String, Daerah> pemira = new HashMap<String, Daerah>();
    static ArrayList<Daerah> provinsi = new ArrayList<Daerah>();

    private static InputReader in;
    private static PrintWriter out;

    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        readInput();
        // System.out.println("AWAL KOTA-KOTA");
        // cekSuara();
        // System.out.println("AKHIRRRRRRRRRR");
        out.close();
    }
    
    private static void readInput() throws IOException {
        donatA = new Donat(in.next());
        donatB = new Donat(in.next());
        long jumlahdaerah = in.nextLong();

        for (long i = 0; i < jumlahdaerah; i++) {
            String kota = in.next();
            Daerah wilayaDaerah;
            if (pemira.containsKey(kota)) {
                wilayaDaerah = pemira.get(kota);
            } else {
                wilayaDaerah = new Daerah(null, kota);
                pemira.put(kota, wilayaDaerah);
                root = AVLTree.insert(root, 0);
            }

            long subdaerah = in.nextLong();
            for (long j = 0; j < subdaerah; j++) {
                String subDaerah = in.next();
                Daerah baru = new Daerah(wilayaDaerah, subDaerah);
                pemira.put(subDaerah, baru);
                root = AVLTree.insert(root, 0);
                if (i == 0) {
                    provinsi.add(baru);
                }
            }
        }
        long banyakPerlongah = in.nextLong();
        for (long i = 0; i < banyakPerlongah; i++) {
            String perintah = in.next();
            if (perintah.equals("TAMBAH")) {
                String daerah = in.next();
                long donA = in.nextLong();
                long donB = in.nextLong();
                pemira.get(daerah).tambahSuara(donA, donB);
            } else if (perintah.equals("ANULIR")) {
                String daerah = in.next();
                long donA = in.nextLong();
                long donB = in.nextLong();
                pemira.get(daerah).kurangSuara(donA, donB);
            }
            else if (perintah.equals("CEK_SUARA")){
                String kota = in.next();
                out.println(pemira.get(kota).cekSuara());
            }
            else if (perintah.equals("WILAYAH_MENANG")){
                wilayahMenang(in.next());
            }
            else if (perintah.equals("CEK_SUARA_PROVINSI")){
                // System.out.println("masuk gasi");
                cekSuaraProvinsi();
                // System.out.println("udah lewat gan");
            }
            else if(perintah.equals("WILAYAH_MINIMAL")){
                String jenisDonat = in.next();
                int persentase = Integer.parseInt(in.next());
                int jumlah = 0;
                if(jenisDonat.equals(donatA.nama)){
                    for(int k = persentase; k<101; k++){
                        jumlah+=donatA.persenMenang[k];
                    }
                }
                else if(jenisDonat.equals(donatB.nama)){
                    for(int k = persentase; k<101; k++){
                        jumlah+=donatB.persenMenang[k];
                    }
                }
                out.println(jumlah);
                // out.println(Arrays.toString(donatA.persenMenang));
                // out.println(Arrays.toString(donatB.persenMenang));
                // System.out.println();
                // cekSuara();
            }
            else if(perintah.equals("WILAYAH_SELISIH")){
                // AVLTree.preOrder(root);
                out.println(AVLTree.recSelisih2(root, in.nextLong()));
            }
        }
    }

    public static void wilayahMenang(String donat){
        if(donat.equals(donatA.nama)){
            out.println(donatA.jumlahMenang);
        }
        else if(donat.equals(donatB.nama)){
            out.println(donatB.jumlahMenang);
        }
        
    }

    public static void cekSuaraProvinsi(){
        // System.out.println("masuk gan");
        for (Daerah prov : provinsi) {
            out.print(prov.name);
            out.print(" ");
            out.println(prov.cekSuara());
        }
    }

    public static void cekSuara(){
        for(String key : pemira.keySet()){
            System.out.println(key + " " + pemira.get(key).cekSuara());
        }
    }

    public static void cekDaerah(){
        System.out.println(pemira.keySet());
    }

    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public long nextLong() {
            return Long.parseLong(next());
        }
 
    }
}

class Daerah{
    Daerah parent = null;
    String name = null;
    String donatMenang;
    long depth = 0;

    long suaraTotal = 0;
    long suaraA = 0;
    long suaraB = 0;
    long selisih = 0;

    long persentaseA = 50;
    long persentaseB = 50;

    Daerah(Daerah bapak, String nama){
        parent = bapak;
        name = nama;
        donatMenang = "seri";
        PemiluDonat.donatA.addPersentase(persentaseA);
        PemiluDonat.donatB.addPersentase(persentaseB);
    } 

    public void tambahA(long jumlah){
        // System.out.println("BAGIAN A");
        // AVLTree.preOrder(PemiluDonat.root);

        PemiluDonat.root = AVLTree.deleteNode(PemiluDonat.root, selisih);

        // System.out.println();
        // AVLTree.preOrder(PemiluDonat.root);

        suaraA+=jumlah;
        suaraTotal+=jumlah;
        selisih = Math.abs(suaraA-suaraB);
        PemiluDonat.root = AVLTree.insert(PemiluDonat.root, selisih);

        // System.out.println();
        // System.out.print("INI SETELAH INSERT ");
        // AVLTree.preOrder(PemiluDonat.root);
        // System.out.println();
        // System.out.println(name + " " + suaraA);
        // System.out.println(name + " " + suaraTotal);

        PemiluDonat.donatA.kurangPersentase(persentaseA);
        PemiluDonat.donatB.kurangPersentase(persentaseB);
        
        if(suaraA==0 && suaraB==0){
            persentaseA = 50;
            persentaseB = 50;
        }
        else if(suaraA == 0){
            persentaseA = 0;
            persentaseB = 100;
        }
        else if(suaraB == 0){
            persentaseA = 100;
            persentaseB = 0;
        }
        else{
            persentaseA = suaraA*100/(suaraA+suaraB);
            persentaseB = suaraB*100/(suaraA+suaraB);
        }

        PemiluDonat.donatA.addPersentase(persentaseA);
        PemiluDonat.donatB.addPersentase(persentaseB);

        if(donatMenang.equals(PemiluDonat.donatA.nama)){
            PemiluDonat.donatA.kurangMenang(1);
        }
        else if(donatMenang.equals(PemiluDonat.donatB.nama)){
            PemiluDonat.donatB.kurangMenang(1);
        }
        
        if(suaraA>suaraB){
            donatMenang=PemiluDonat.donatA.nama;
            PemiluDonat.donatA.tambahMenang(1);
        }
        else if(suaraA<suaraB){
            donatMenang=PemiluDonat.donatB.nama;
            PemiluDonat.donatB.tambahMenang(1);
        }
        else{
            donatMenang = "seri";
        }

        if(parent!=null){
            parent.tambahA(jumlah);
        }
    }
    
    public void tambahB(long jumlah){
        // System.out.println("BAGIAN B");
        // AVLTree.preOrder(PemiluDonat.root);

        PemiluDonat.root = AVLTree.deleteNode(PemiluDonat.root, selisih);

        // System.out.println();
        // AVLTree.preOrder(PemiluDonat.root);

        suaraB+=jumlah;
        suaraTotal+=jumlah;
        selisih = Math.abs(suaraA-suaraB);
        PemiluDonat.root = AVLTree.insert(PemiluDonat.root, selisih);

        // System.out.println();
        // System.out.print("INI SETELAH INSERT ");
        // AVLTree.preOrder(PemiluDonat.root);
        // System.out.println();
        // System.out.println(name + " " + suaraB);
        // System.out.println(name + " " + suaraTotal);

        PemiluDonat.donatA.kurangPersentase(persentaseA);
        PemiluDonat.donatB.kurangPersentase(persentaseB);
        
        if(suaraA==0 && suaraB==0){
            persentaseA = 50;
            persentaseB = 50;
        }
        else if(suaraA == 0){
            persentaseA = 0;
            persentaseB = 100;
        }
        else if(suaraB == 0){
            persentaseA = 100;
            persentaseB = 0;
        }
        else{
            persentaseA = suaraA*100/(suaraA+suaraB);
            persentaseB = suaraB*100/(suaraA+suaraB);
        }

        PemiluDonat.donatA.addPersentase(persentaseA);
        PemiluDonat.donatB.addPersentase(persentaseB);

        if(donatMenang.equals(PemiluDonat.donatA.nama)){
            PemiluDonat.donatA.kurangMenang(1);
        }
        else if(donatMenang.equals(PemiluDonat.donatB.nama)){
            PemiluDonat.donatB.kurangMenang(1);
        }
        
        if(suaraA>suaraB){
            donatMenang=PemiluDonat.donatA.nama;
            PemiluDonat.donatA.tambahMenang(1);
        }
        else if(suaraA<suaraB){
            donatMenang=PemiluDonat.donatB.nama;
            PemiluDonat.donatB.tambahMenang(1);
        }
        else{
            donatMenang = "seri";
        }
        if(parent!=null){
            parent.tambahB(jumlah);
        }
    }

    public void tambahSuara(long jumlahA, long jumlahB){
        tambahA(jumlahA);
        // System.out.println();
        tambahB(jumlahB);
        // System.out.println();
    }

    public void kurangA(long jumlah){
        PemiluDonat.root = AVLTree.deleteNode(PemiluDonat.root, selisih);
        suaraA-=jumlah;
        suaraTotal-=jumlah;
        selisih = Math.abs(suaraA-suaraB);
        PemiluDonat.root = AVLTree.insert(PemiluDonat.root, selisih);

        PemiluDonat.donatA.kurangPersentase(persentaseA);
        PemiluDonat.donatB.kurangPersentase(persentaseB);
        
        if(suaraA==0 && suaraB==0){
            persentaseA = 50;
            persentaseB = 50;
        }
        else if(suaraA == 0){
            persentaseA = 0;
            persentaseB = 100;
        }
        else if(suaraB == 0){
            persentaseA = 100;
            persentaseB = 0;
        }
        else{
            persentaseA = suaraA*100/(suaraA+suaraB);
            persentaseB = suaraB*100/(suaraA+suaraB);
        }

        PemiluDonat.donatA.addPersentase(persentaseA);
        PemiluDonat.donatB.addPersentase(persentaseB);

        if(donatMenang.equals(PemiluDonat.donatA.nama)){
            PemiluDonat.donatA.kurangMenang(1);
        }
        else if(donatMenang.equals(PemiluDonat.donatB.nama)){
            PemiluDonat.donatB.kurangMenang(1);
        }
        
        if(suaraA>suaraB){
            donatMenang=PemiluDonat.donatA.nama;
            PemiluDonat.donatA.tambahMenang(1);
        }
        else if(suaraA<suaraB){
            donatMenang=PemiluDonat.donatB.nama;
            PemiluDonat.donatB.tambahMenang(1);
        }
        else{
            donatMenang = "seri";
        }
        if(parent!=null){
            parent.kurangA(jumlah);
        }
    }
    
    public void kurangB(long jumlah){
        PemiluDonat.root = AVLTree.deleteNode(PemiluDonat.root, selisih);
        suaraB-=jumlah;
        suaraTotal-=jumlah;
        selisih = Math.abs(suaraA-suaraB);
        PemiluDonat.root = AVLTree.insert(PemiluDonat.root, selisih);

        PemiluDonat.donatA.kurangPersentase(persentaseA);
        PemiluDonat.donatB.kurangPersentase(persentaseB);
        
        if(suaraA==0 && suaraB==0){
            persentaseA = 50;
            persentaseB = 50;
        }
        else if(suaraA == 0){
            persentaseA = 0;
            persentaseB = 100;
        }
        else if(suaraB == 0){
            persentaseA = 100;
            persentaseB = 0;
        }
        else{
            persentaseA = suaraA*100/(suaraA+suaraB);
            persentaseB = suaraB*100/(suaraA+suaraB);
        }

        PemiluDonat.donatA.addPersentase(persentaseA);
        PemiluDonat.donatB.addPersentase(persentaseB);

        if(donatMenang.equals(PemiluDonat.donatA.nama)){
            PemiluDonat.donatA.kurangMenang(1);
        }
        else if(donatMenang.equals(PemiluDonat.donatB.nama)){
            PemiluDonat.donatB.kurangMenang(1);
        }
        
        if(suaraA>suaraB){
            donatMenang=PemiluDonat.donatA.nama;
            PemiluDonat.donatA.tambahMenang(1);
        }
        else if(suaraA<suaraB){
            donatMenang=PemiluDonat.donatB.nama;
            PemiluDonat.donatB.tambahMenang(1);
        }
        else{
            donatMenang = "seri";
        }
        if(parent!=null){
            parent.kurangB(jumlah);
        }
    }

    public void kurangSuara(long jumlahA, long jumlahB){
        kurangA(jumlahA);
        kurangB(jumlahB);
    }

    public String cekSuara(){
        return suaraA + " " + suaraB;
    }

    public String pemenang(){
        return donatMenang;
    }

}

class Donat{
    String nama;
    long jumlahMenang = 0;
    int[] persenMenang = new int[101];

    Donat(String namaDonat){
        nama = namaDonat;
    }

    public void tambahMenang(long angka){
        jumlahMenang+=angka;
    }
    
    public void kurangMenang(long angka){
        jumlahMenang-=angka;
    }

    public void addPersentase(Long persen){
        int persentase = persen.intValue();
        persenMenang[persentase]+=1;
    }
    
    public void kurangPersentase(Long persen){
        int persentase = persen.intValue();
        persenMenang[persentase]-=1;
    }
}


// An AVL tree node 
class Node { 
    long key;
    Node left;
    Node right;

    int height;
    int count;
    long anak;
} 

// Java program of AVL tree that handles duplicates 
class AVLTree { 

	// A utility function to get height of the tree 
	static int height(Node N){ 
		if (N == null) 
			return 0; 
		return N.height; 
	} 

	// A utility function to get maximum of two integers 
	static int max(int a, int b) { 
		return (a > b) ? a : b; 
    }
    
    // static Node kurangAnak(Node N, long selisih){
    //     if(N.key == selisih){
    //         return null;
    //     }
    //     else if(N.key > selisih){
    //         N.anakKiri--;
    //         return kurangAnak(N.left, selisih);
    //     }
    //     else{
    //         N.anakKanan--;
    //         return kurangAnak(N.right, selisih);
    //     }
    // }

	/* Helper function that allocates a new node with the given key and 
	null left and right pointers. */
	static Node newNode(long key){ 
		Node node = new Node(); 
		node.key = key; 
		node.left = null; 
		node.right = null; 
		node.height = 1; // new node is initially added at leaf 
        node.count = 1;
        node.anak = 1;
		return (node); 
	} 

	// A utility function to right rotate subtree rooted with y 
	// See the diagram given above. 
	static Node rightRotate(Node y){ 
		Node x = y.left; 
		Node T2 = x.right; 

        y.anak+=(x.right == null ? 0 : x.right.anak)-x.anak;
		// Perform rotation 
		x.right = y; 
		y.left = T2; 
        x.anak = x.count + (x.left == null ? 0 : x.left.anak) + (x.right == null ? 0 : x.right.anak);
        

		// Update heights 
		y.height = max(height(y.left), height(y.right)) + 1; 
		x.height = max(height(x.left), height(x.right)) + 1; 

		// Return new root 
		return x; 
	} 

	// A utility function to left rotate subtree rooted with x 
	// See the diagram given above. 
	static Node leftRotate(Node x){ 
		Node y = x.right; 
		Node T2 = y.left; 

        x.anak+=(y.left == null ? 0 : y.left.anak)-y.anak;
		// Perform rotation 
		y.left = x; 
        x.right = T2; 
        y.anak = y.count + (y.left == null ? 0 : y.left.anak) + (y.right == null ? 0 : y.right.anak);
        
		// Update heights 
		x.height = max(height(x.left), height(x.right)) + 1; 
		y.height = max(height(y.left), height(y.right)) + 1; 

		// Return new root 
		return y; 
	} 

	// Get Balance factor of node N 
	static int getBalance(Node N){ 
		if (N == null) 
			return 0; 
		return height(N.left) - height(N.right); 
	} 

	static Node insert(Node node, long selisih) {
        /* 1. Perform the normal BST rotation */
        if (node == null)
            return (newNode(selisih));

        // If key already exists in BST, increment count and return
        if (selisih == node.key) {
            (node.count)++;
            node.anak++;
            return node;
        }

        /* Otherwise, recur down the tree */
        if (selisih < node.key){
            node.anak++;
            node.left = insert(node.left, selisih);
        }
        else{
            node.anak++;
            node.right = insert(node.right, selisih);

        }

        /* 2. Update height of this ancestor node */
        node.height = max(height(node.left), height(node.right)) + 1;

        /*
         * 3. Get the balance factor of this ancestor node to check whether this node
         * became unbalanced
         */
        int balance = getBalance(node);

        // If this node becomes unbalanced, then there are 4 cases

        // Left Left Case
        if (balance > 1 && selisih < node.left.key)
            return rightRotate(node);

        // Right Right Case
        if (balance < -1 && selisih > node.right.key)
            return leftRotate(node);

        // Left Right Case
        if (balance > 1 && selisih > node.left.key) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && selisih < node.right.key) {
			node.right = rightRotate(node.right); 
			return leftRotate(node); 
		} 

		/* return the (unchanged) node pointer */
		return node; 
	} 

	/* Given a non-empty binary search tree, return the node with minimum 
key value found in that tree. Note that the entire tree does not 
need to be searched. */
	static Node minValueNode(Node node) 
	{ 
		Node current = node; 

		/* loop down to find the leftmost leaf */
		while (current.left != null) 
			current = current.left; 

		return current; 
	} 

	static Node deleteNode(Node root, long selisih) {
        // STEP 1: PERFORM STANDARD BST DELETE

        if (root == null)
            return root;

        // If the key to be deleted is smaller than the root's key,
        // then it lies in left subtree
        if (selisih < root.key){
            root.anak--;
            root.left = deleteNode(root.left, selisih);
            root.anak = (root.left == null ? 0 : root.left.anak) + (root.right == null ? 0 : root.right.anak) + root.count;
            
        }

        // If the key to be deleted is greater than the root's key,
        // then it lies in right subtree
        else if (selisih > root.key){
            root.anak--;
            root.right = deleteNode(root.right, selisih);
            root.anak = (root.left == null ? 0 : root.left.anak) + (root.right == null ? 0 : root.right.anak) + root.count;
        }

		// if key is same as root's key, then This is the node 
		// to be deleted 
		else { 
			// If key is present more than once, simply decrement 
			// count and return 
			if (root.count > 1) { 
                (root.count)--; 
                root.anak--;
				return root; 
			} 
            // ElSE, delete the node 
            
			// node with only one child or no child 
			if ((root.left == null) || (root.right == null)) { 
				Node temp = root.left != null ? root.left : root.right; 

				// No child case 
				if (temp == null) { 
					temp = root; 
					root = null; 
				} 
				else // One child case 
					root = temp; // Copy the contents of the non-empty child 
			} 
			else { 
				// node with two children: Get the inorder successor (smallest 
				// in the right subtree) 
				Node temp = minValueNode(root.right); 

				// Copy the inorder successor's data to this node and update the count 
				root.key = temp.key; 
				root.count = temp.count; 
				temp.count = 1;

				// Delete the inorder successor 
                root.right = deleteNode(root.right, temp.key);
                root.anak = (root.left == null ? 0 : root.left.anak) + (root.right == null ? 0 : root.right.anak) + root.count;
			} 
		} 

		// If the tree had only one node then return 
		if (root == null) 
			return root; 

		// STEP 2: UPDATE HEIGHT OF THE CURRENT NODE 
		root.height = max(height(root.left), height(root.right)) + 1; 

		// STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether 
		// this node became unbalanced) 
		int balance = getBalance(root); 

		// If this node becomes unbalanced, then there are 4 cases 

		// Left Left Case 
		if (balance > 1 && getBalance(root.left) >= 0) 
			return rightRotate(root); 

		// Left Right Case 
		if (balance > 1 && getBalance(root.left) < 0) { 
			root.left = leftRotate(root.left); 
			return rightRotate(root); 
		} 

		// Right Right Case 
		if (balance < -1 && getBalance(root.right) <= 0) 
			return leftRotate(root); 

		// Right Left Case 
		if (balance < -1 && getBalance(root.right) > 0) { 
			root.right = rightRotate(root.right); 
			return leftRotate(root); 
		} 

		return root; 
	} 

	// A utility function to print preorder traversal of the tree. 
	// The function also prints height of every node 
	static void preOrder(Node root){ 
		if (root != null) { 
			System.out.printf("%d(%d)", root.key, root.count); 
			preOrder(root.left); 
			preOrder(root.right); 
		} 
    } 

    static void inOrder(Node root){ 
		if (root != null) { 
            preOrder(root.left); 
			System.out.printf("%d(%d)[%d] ", root.key, root.count, root.anak); 
			preOrder(root.right); 
		} 
    }

    static long recSelisih(Node akar, long angka){
        if (akar == null) {
            return 0;
          }
          long countLeft = recSelisih(akar.left, angka);
          long countRight = recSelisih(akar.right, angka);
        
          return (akar.key >= angka ? akar.count : 0) + countLeft + countRight;
    }

    static long recSelisih2(Node akar, long angka){
        if(akar==null){
            return 0;
        }
        else if (akar.key==angka) {
            // System.out.println("preord");
            // preOrder(akar.right);
            // System.out.println("inord");
            // inOrder(akar.right);
            return akar.count + (akar.right == null ? 0 : akar.right.anak);
        }
        else if(akar.key<angka){
            return recSelisih2(akar.right, angka);
        }
        else{
            return akar.count + (akar.right == null ? 0 : akar.right.anak) + recSelisih2(akar.left, angka);
        }
    }

} 