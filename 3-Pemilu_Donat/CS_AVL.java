class Node{
    long value;
    long banyak;
    long jumlahKanan, jumlahKiri;

    Node left;
    Node right;
    int height = 0;

    public Node(long selisih, Node left, Node right){
        this.value = selisih;
        this.left = left;
        this.right = right;
        height = 0;
        banyak = 1;
    }

    public int getHeight(){
        return height;
    }

    public void updateHeight(){
        int tinggiKiri = (left==null) ? 0:left.height;
        int tinggiKanan = (right==null) ? 0:right.height;
        height = Math.max(tinggiKanan, tinggiKiri) + 1;
    }

    public boolean isBalance(){
        return Math.abs(getBalanceFactor()) <=1;
    }

    public int getBalanceFactor(){
        int tinggiKiri = (left==null) ? 0:left.height;
        int tinggiKanan = (right==null) ? 0:right.height;
        return tinggiKiri-tinggiKanan;    
    }
}

public class CS_AVL{
    Node root;

    public CS_AVL(){
        root = null;
    }

    public static void main(String[] args) {
        CS_AVL pohon = new CS_AVL();
        pohon.root = pohon.insert(10, pohon.root);
        pohon.root = pohon.insert(20, pohon.root);
        pohon.root = pohon.insert(30, pohon.root);
        pohon.root = pohon.insert(40, pohon.root);
        pohon.root = pohon.insert(50, pohon.root);
        pohon.root = pohon.insert(25, pohon.root);
        
        pohon.preOrder(pohon.root);

        CS_AVL tree = new CS_AVL();
        tree.root = tree.insert(13, tree.root);  
        tree.root = tree.insert(7, tree.root);  
        tree.root = tree.insert(10, tree.root);  
        tree.root = tree.insert(10, tree.root);  
        tree.root = tree.insert(8, tree.root);  
        tree.root = tree.insert(6, tree.root);  
        tree.root = tree.insert(6, tree.root);  
        tree.root = tree.insert(8, tree.root);  
        tree.root = tree.insert(10, tree.root);
        tree.root = tree.insert(8, tree.root);
        tree.root = tree.insert(8, tree.root);
        tree.root = tree.insert(6, tree.root);
        tree.root = tree.insert(2, tree.root);

        // for(int i = 0; i<13; i++){
        //     tree.insert(0);
        // }
        // tree.deleteNode(tree.root, 0);
        // tree.insert(5);


        System.out.println();
        tree.preOrder(tree.root);
        System.out.println("SESUDAH");
        tree.deleteNode(tree.root, 9);  
        tree.preOrder(tree.root);
        System.out.println("EFEK");
        System.out.println(tree.jumlahSelisihMinimal(7));
    }

    public void preOrder(Node node) { 
        if (node != null) { 
            // System.out.print(node.value + " "); 
            System.out.printf("%d(%d) ", node.value, node.banyak);
            preOrder(node.left); 
            preOrder(node.right); 
        } 
    }
    
    public long jumlahSelisihMinimal(long angka){
        return recSelisih(root, angka);
    }

    private long recSelisih(Node akar, long angka){
        if(akar==null){
            return 0;
        }
        else if(akar.value>angka){
            return  akar.banyak + recSelisih(akar.left, angka) 
            + recSelisih(akar.right, angka);
        }
        else if(akar.value<angka){
            return recSelisih(akar.right, angka);
        }
        else{
            return akar.banyak + recSelisih(akar.right, angka);
        }
    }

    public int getHeight(Node N){
        if(N==null){
            return 0;
        }
        return N.getHeight();
    }

    public void insert(long baru){
        root = insert(baru, root);
    }

    private Node insert(long baru, Node t){
        if(t==null){
            t = new Node(baru, null, null);
        }
        else if(baru<t.value){
            t.left = insert(baru, t.left);
            if(!t.isBalance()){
                if(baru<t.left.value){
                    t = singleRotateWithLeftChild(t);
                }
                else{
                    t = doubleRotateWithLeftChild(t);
                }
            }
        }
        else if(baru>t.value){
            t.right = insert(baru, t.right);
            if(!t.isBalance()){
                if(baru>t.right.value) {
                    t = singleRotateWithRightChild(t);
                } 
                else{                    
                    t = doubleRotateWithRightChild(t);
                }
            }
        }
        else if(baru == t.value){
            t.banyak ++;
            return t;
        }

        t.updateHeight();
        return t;
    }

    /* Given a non-empty binary search tree, return the  
    node with minimum key value found in that tree.  
    Note that the entire tree does not need to be  
    searched. */
    Node minValueNode(Node node)  
    {  
        Node current = node;  
  
        /* loop down to find the leftmost leaf */
        while (current.left != null)  
        current = current.left;  
  
        return current;  
    }  
  
    Node deleteNode(Node root, long value)  
    {  
        // STEP 1: PERFORM STANDARD BST DELETE  
        if (root == null)  
            return root;  
  
        // If the key to be deleted is smaller than  
        // the root's key, then it lies in left subtree  
        if (value < root.value)  
            root.left = deleteNode(root.left, value);  
  
        // If the key to be deleted is greater than the  
        // root's key, then it lies in right subtree  
        else if (value > root.value)  
            root.right = deleteNode(root.right, value);  
  
        // if key is same as root's key, then this is the node  
        // to be deleted  
        else{  

            if(root.banyak>1) {
                root.banyak--;
                return null;
            }
            // node with only one child or no child  
            if ((root.left == null) || (root.right == null))  
            {  
                Node temp = null;  
                if (temp == root.left){
                    temp = root.right;  
                }  
                else{
                    temp = root.left;  
                }
  
                // No child case  
                if (temp == null){  
                    temp = root;  
                    root = null;  
                }  
                else{
                    // One child case  
                    root = temp; // Copy the contents of  
                                // the non-empty child  
                } 
            }  
            else{  
                // node with two children: Get the inorder  
                // successor (smallest in the right subtree)  
                Node temp = minValueNode(root.right);  
  
                // Copy the inorder successor's data to this node  
                root.value = temp.value;  
  
                // Delete the inorder successor  
                root.right = deleteNode(root.right, temp.value);  
            }  
        }  

        // If the tree had only one node then return  
        if (root == null)  
            return root;  
  
        // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE  
        root.height = Math.max(getHeight(root.left), getHeight(root.right)) + 1;  
  
        // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether  
        // this node became unbalanced)  
        int balance = root.getBalanceFactor();  
  
        // If this node becomes unbalanced, then there are 4 cases  
        // Left Left Case  
        if (balance > 1 && root.left.getBalanceFactor() >= 0){
            return singleRotateWithLeftChild(root);  
        }  
  
        // Left Right Case  
        if (balance > 1 && root.left.getBalanceFactor() < 0){  
            root = doubleRotateWithRightChild(root);  
            return root;
        }  
  
        // Right Right Case  
        if (balance < -1 && root.right.getBalanceFactor() <= 0){
            return singleRotateWithRightChild(root);  
        }
  
        // Right Left Case  
        if (balance < -1 && root.right.getBalanceFactor() > 0){  
            root = doubleRotateWithRightChild(root);  
            return root;  
        }  
  
        return root;  
    }  

    // Rotate 1 Kiri
    /**
    To Single Rotate Left
    */
    private static Node singleRotateWithRightChild(Node k1){
        Node k2 = k1.right;
        k1.right = k2.left;
        k2.left = k1;
        k1.updateHeight();
        k2.updateHeight();
        return k2;
    }

    //Rotate 1 Kanan
    /**
    To Single Rotate Right
    */
    private static Node singleRotateWithLeftChild(Node k2){
        Node k1 = k2.left;
        k2.left = k1.right;
        k1.right = k2;
        k2.updateHeight();
        k1.updateHeight();
        return k1;
    }

    /**
    To Double Rotate Left
    */
    private static Node doubleRotateWithLeftChild(Node k3){
        k3.left = singleRotateWithRightChild(k3.left);
        return singleRotateWithLeftChild(k3);
    }

    /**
    To Double Rotate Right
    */
    private static Node doubleRotateWithRightChild(Node k3){
        k3.right = singleRotateWithLeftChild(k3.right);
        return singleRotateWithRightChild(k3);
    }
}