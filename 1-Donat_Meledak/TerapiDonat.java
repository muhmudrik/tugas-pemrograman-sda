import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class TerapiDonat {
    private static InputReader in;
    private static PrintWriter out;
    static HashMap<String, Toko> listToko = new HashMap<String, Toko>();
    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);
        loadInput();
        out.close();
    }
    
    private static void printOutput(int answer) throws IOException {
        out.println(answer);
    }
    
    private static void loadInput() throws IOException {
        // Jumlah toko
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            String namaToko = in.next();
            Toko toko = new Toko(namaToko);
            // Jumlah jenis donat
            listToko.put(namaToko, toko);
            int q =  in.nextInt();
            for (int j = 0; j < q ; j++) {
                // Disini index 0 = stok
                // Lalu index 1 = chip
                toko.getDonat().put(in.next(), new int[]{in.nextInt(),in.nextInt()});
            }
        }   
        
        ArrayList<Toko> tokoBuka = new ArrayList<Toko>();
        int target;
        // Jumlah hari jualan
        int hari = in.nextInt();
        for (int i = 0; i < hari; i++) {
            //Toko yang buka
            int jumlahTokoBuka = in.nextInt();
            if(jumlahTokoBuka>0){
                for (int j = 0; j < jumlahTokoBuka; j++) {
                    tokoBuka.add(listToko.get(in.next()));
                }
            }
            in.next();
            target = in.nextInt();
            // Proses hitungan kemungkinan cara beli donat here
            if(jumlahTokoBuka==0 && target==0){
                printOutput(1);
            }
            else if(jumlahTokoBuka==0){
                printOutput(0);
            }
            else{
                printOutput(mungkinDonat(target, tokoBuka));
            }
            tokoBuka.clear();

            // Donat meledak dimana
            in.next();
            duar();
            
            // Restock
            in.next();
            restock();

            // Transfer
            in.next();
            transfer();
        }
    }

    public static int mungkinDonat(int target, ArrayList<Toko> tokoBuka{
        if(target==0){
            return 1;
        }
        ArrayList<int[]> donatTersedia= new ArrayList<int[]>();
        int[] poliDonat;
        for (Toko tempToko : tokoBuka) {
            for (String var : tempToko.getDonat().keySet()) {
                poliDonat = new int[target+1];
                int stok = tempToko.getDonat().get(var)[0];
                int chips = tempToko.getDonat().get(var)[1];
                for (int i = 0; i < poliDonat.length; i++) {
                    if(i==0){
                        poliDonat[i] = 1;
                    }
                    else if(i%chips==0 && i<=stok*chips){
                        poliDonat[i] = 1;
                    }
                    else{
                        poliDonat[i] = 0;
                    }
                }
                donatTersedia.add(poliDonat);
            }
        }

        int[] hasil = new int[target+1];
        Arrays.fill(hasil, 0);
        hasil[0] = 1;
        for (int[] tempPoliDonat : donatTersedia) {
            int [] kalikan = kaliPoli(hasil, tempPoliDonat, target);
            hasil = kalikan;
        }
        return hasil[target];
    }

    public static int[]  kaliPoli(int[] A, int[] B, int m){
        int[] hasil = new int[m+1];
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= m; j++) {
                if(!(i+j>m)){
                    hasil[i+j] += A[i] * B[j];
                    hasil[i+j] %= 1000000007;
                }     
            }
        }
        return hasil;
    }

    public static void duar(){
        int jumlahDuar = in.nextInt();

        for(int i = 0; i < jumlahDuar ; i++){
            String namaToko = in.next();
            String jenisDonat = in.next();
            int jumlahLedak = in.nextInt();
            listToko.get(namaToko).getDonat().get(jenisDonat)[0]-=jumlahLedak;

            if(listToko.get(namaToko).getDonat().get(jenisDonat)[0]<=0){
                listToko.get(namaToko).getDonat().remove(jenisDonat);    
            }
        }
    }

    public static void restock(){
        int jumlahTokoRestock = in.nextInt();
        for (int i = 0; i < jumlahTokoRestock; i++) {
            Toko tokores = listToko.get(in.next());
            String jenisDonat = in.next();
            int jumlahRestock = in.nextInt();
            int jumlahChips = in.nextInt();
            
            if(!tokores.getDonat().containsKey(jenisDonat) || tokores.getDonat().get(jenisDonat)[0]==0){
                tokores.getDonat().put(jenisDonat, new int[]{jumlahRestock, jumlahChips});
            }
            // Kalau jumlah chip sama, tambahin, kalau enggak biarin aja
            else if(tokores.getDonat().get(jenisDonat)[1] == jumlahChips){
                tokores.getDonat().get(jenisDonat)[0] += jumlahRestock;
            }
        }
    }

    public static void transfer(){
        int jumlahTokoTransfer = in.nextInt(); 
        for (int i = 0; i < jumlahTokoTransfer; i++) {
            Toko toko1 = listToko.get(in.next());
            Toko toko2 = listToko.get(in.next());
            String donat = in.next();
            int jumlahDonat = in.nextInt();
            int chipsDonat = toko1.getDonat().get(donat)[1];
            if(!toko2.getDonat().containsKey(donat)){
                toko2.getDonat().put(donat, new int[]{jumlahDonat, chipsDonat});
                toko1.getDonat().get(donat)[0] -= jumlahDonat;
            }
            else if(chipsDonat == toko2.getDonat().get(donat)[1]){
                toko1.getDonat().get(donat)[0] -= jumlahDonat;
                toko2.getDonat().get(donat)[0] += jumlahDonat;
            }
            
            if(toko1.getDonat().get(donat)[0]<=0){
                toko1.getDonat().remove(donat);    
            }
        }
    }
    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;
 
        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }
 
        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
    }
}

class Toko{
	private String nama;
	HashMap<String, int[]> jenisDonat = new HashMap<String, int[]>();
	// Index 1 untuk kuantitas donat, index 0 untuk chip
	Toko(String nama){
		this.nama = nama;
    }
    
    public String getNama(){
        return this.nama;
    }

    public HashMap<String, int[]> getDonat(){
        return this.jenisDonat;
    }

    public String toString(){
        String tempString = "" + this.nama;
        for (String name: this.jenisDonat.keySet()){
            String key = name.toString();
            String value = Arrays.toString(this.jenisDonat.get(name));  
            tempString +=" " + key + "=" + value + ", ";   
        } 
        return tempString;
    }
}


/*

Credit to Zafir for the idea of this alghoritm
Credit to GeeksforGeeks for the polynomial multiplication idea

*/