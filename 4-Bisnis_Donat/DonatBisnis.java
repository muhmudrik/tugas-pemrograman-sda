import java.io.*;
import java.lang.reflect.Array;
import java.util.*;


/**
 * Data Structure and Alghoritm Project Assignment 4, about Graph
 * 
 * Created by:
 * Muhammad Mudrik - 1806190935 - Faculty of Computer Science, Universitas Indonesia
 * 
 * This code cant be done without the help of
 * 1. GeeksForGeeks, especially Shubham Agrawal and Shlomi Elhaiani
 * 2. Some advice from my friends about technical problem
 * 3. My Self for the hardwork all day and night
 * 4. Graph BFS and DFS Material from Scele Fasilkom UI
 */



class Graph{
    private static int JUMLAH_VERTEX;
    private static Vertex[] adjList; 
  

    Graph(int v){
        JUMLAH_VERTEX = v; 
        adjList = new Vertex[JUMLAH_VERTEX+1]; 
        for (int i=0; i<=JUMLAH_VERTEX; i++) 
            adjList[i] = new Vertex(i); 
    }

    void printGraph(PrintWriter out){
        for(int i=1; i<=JUMLAH_VERTEX; i++){
            out.print(i+" => ");
            int anakKe=0;
            while(anakKe<adjList[i].koneksi.size()){
                out.print(adjList[i].koneksi+" ");
                anakKe++;
            }
            out.println("");
        }
    }

    void addEdge(int v, int w, int weight){
        Edge buatV = new Edge(v, weight, w, adjList[w]);
        Edge buatW = new Edge(w, weight, v, adjList[v]);
        adjList[v].addEdge(buatV);
        adjList[w].addEdge(buatW);
    } 

    // -------------------------------------------------------------------------------------------------------------------------

    /**
     * Could be Improved, See slide and use queue
     * @param kotaBuka
     */
    // Open Store
    int openStore(ArrayList<Integer> kotaBuka){
        Set<Integer> hasil = new HashSet<Integer>();
        ArrayList<Edge> visited = new ArrayList<Edge>();
        List<Edge> temp;
        if(kotaBuka.size()==0){
            return -1;
        }
        for(int i = 0; i<kotaBuka.size(); i++){
            temp = adjList[kotaBuka.get(i)].koneksi;
            if(temp.size()==0){
                hasil.add(kotaBuka.get(i));
                continue;
            }
            if(hasil.contains(kotaBuka.get(i))){
                continue;
            }
            for(int j = 0; j<temp.size(); j++){
                // System.out.println(adjList[kotaBuka.get(i)].koneksi);
                visited.add(temp.get(j));
                hasil.add(temp.get(j).kotaAsal);
                getRangeOS(temp.get(j), visited, hasil);
            }
        }
        // System.out.println(hasil);
        return hasil.size();
    }

    int getRangeOS(Edge V, ArrayList<Edge> visited, Set<Integer> hasil ){
        if(hasil.contains(V.kotaTujuan)){
            // Sudah pernah
            return 1;
        }
        if(!hasil.contains(V.kotaTujuan)){
            visited.add(V);
            hasil.add(V.kotaTujuan);
        }
        List<Edge> temporary = adjList[V.kotaTujuan].koneksi;
        for(int i = 0; i<temporary.size(); i++){
            getRangeOS(temporary.get(i), visited, hasil);
        }
        return 1;
    }

    // -------------------------------------------------------------------------------------------------------------------------


    // Count City with given distance
    int getCCWGD(int kotaStart, int weightMax){
        Vertex start = adjList[kotaStart];
        ArrayList<Edge> visited = new ArrayList<Edge>();
        Set<Vertex> hasil = new HashSet<Vertex>();
        // System.out.println(start.koneksi);
        for(int i=0; i<start.koneksi.size(); i++){
            Edge temp = start.koneksi.get(i);
            // System.out.println("sedang di asal "+ temp.kotaAsal + " ke " + temp.kotaTujuan + " weight -> " + temp.weight);
            
            deepRDFS(temp, visited, weightMax, hasil);
        }
        // System.out.println(hasil);
        if(hasil.size()==0){
            return 0;
        }
        return hasil.size()-1;
    }

    int deepRDFS(Edge start, ArrayList<Edge> visited, int weightMax, Set<Vertex> hasil){
        if(visited.contains(start)){
            return 1;
        }
        if(start.weight<=weightMax && !visited.contains(start)){
            visited.add(start);
            hasil.add(adjList[start.kotaAsal]);
            List<Edge> tempo = adjList[start.kotaTujuan].koneksi;
            // System.out.println("masuk woi");
            // System.out.println(tempo);
            for(int i=0; i<tempo.size(); i++){
                // System.out.println("Masuk ke " + tempo.get(i));
                deepRDFS(tempo.get(i), visited, weightMax, hasil);
            }
        }
        return 0;
    }

    // -------------------------------------------------------------------------------------------------------------------------

    int laorBFS(int start, int destination){
        Vertex source = adjList[start];
        boolean[] visited = new boolean[JUMLAH_VERTEX+1];
        int[] path = new int[JUMLAH_VERTEX+1];
        Queue<Vertex> queue = new LinkedList<Vertex>();
        
        visited[source.nama] = true;
        queue.add(source);

        while (queue.size()!=0) {
            source = queue.poll();
            // System.out.println(source.nama+ " ");

            Iterator<Edge> i = adjList[source.nama].koneksi.listIterator();
            while(i.hasNext()){
                int n = i.next().kotaTujuan;
                if(!visited[n]){
                    visited[n]=true;
                    path[n] = source.nama;
                    queue.add(adjList[n]);
                }
            }
        }
        // System.out.println("Sampe sini");
        int hasil = 0;
        while(destination!=0){
            hasil++;
            destination = path[destination];
        }
        // System.out.println("selesai itung");
        if(hasil==1){
            return -1;
        }
        return hasil-1;
    }

    int laorcBFS(int start, int destination){
        // System.out.println("THi loop end heye");
        int[] dist =  new int[JUMLAH_VERTEX+1];
        int[] path =  new int[JUMLAH_VERTEX+1];
        Arrays.fill(dist, Integer.MAX_VALUE);
        Arrays.fill(path, 0);

        boolean[] visited = new boolean[JUMLAH_VERTEX+1];

        dist[start] = 0;
        path[start] = 1;

        Queue<Integer> queue = new LinkedList<Integer>();

        queue.add(start);
        visited[start] = true;

        while(!queue.isEmpty()){
            int curr = queue.poll();

            for(Edge temp : adjList[curr].koneksi){
                int x = temp.kotaTujuan;
                if(!visited[x]){
                    queue.add(x);
                    visited[x] = true;
                }

                if(dist[x] > dist[curr] +1 ){
                    dist[x] = dist[curr] +1;
                    path[x] = path[curr];
                }
                else if(dist[x] == dist[curr]+1){
                    path[x] += path[curr];
                    path[x] = path[x] %10001;
                }
            }
        }

        return path[destination] == Integer.MAX_VALUE ? 0 : path[destination];
    }

    int findSR(int start, int destination){
        int[] dist = new int[JUMLAH_VERTEX+1];
        djikstra(dist, start);
        // System.out.println(Arrays.toString(dist));
        if(dist[destination]==Integer.MAX_VALUE){
            return -1;
        }
        return dist[destination];
    }

    int findSM(int borman, int norman){
        int[] distBor = new int[JUMLAH_VERTEX+1];
        int[] distNor = new int[JUMLAH_VERTEX+1];
        
        djikstra(distBor, borman);
        djikstra(distNor, norman);
        // System.out.println();
        // System.out.println(Arrays.toString(distBor));
        // System.out.println(Arrays.toString(distNor));

        int miniMax = distBor[0];
        for(int i = 1; i<JUMLAH_VERTEX+1; i++){
            if(distBor[i]!=Integer.MAX_VALUE && distNor[i]!=Integer.MAX_VALUE ){
                int tempMax = (distBor[i] >= distNor[i]) ? distBor[i] : distNor[i];
                miniMax = (miniMax <= tempMax) ? miniMax : tempMax;
            }
        }
        return miniMax!=Integer.MAX_VALUE ? miniMax : -1;
        
    }

    public void djikstra(int[] dist, int src){
        PriorityQueue<Edge> pq = new PriorityQueue<Edge>();
        Set<Integer> settled = new HashSet<Integer>();
        Arrays.fill(dist, Integer.MAX_VALUE);

        pq.add(new Edge(src, 0));

        dist[src] = 0;

        // while(settled.size()!=JUMLAH_VERTEX-1){
        while(pq.size()!=0){
            // System.out.println(pq);
            int u = pq.remove().kotaAsal;

            settled.add(u);

            int edgeDistance = -1;
            int newDistance = -1;

            for(int i = 0; i<adjList[u].koneksi.size(); i++){
                // System.out.println(Arrays.toString(dist));
                Edge v = adjList[u].koneksi.get(i);
                // System.out.println("asal"+v.kotaAsal+" tujuan" + v.kotaTujuan);
                if(!settled.contains(v.kotaTujuan)){
                    edgeDistance = v.weight;
                    newDistance = dist[u] + edgeDistance;
                    
                    if(newDistance < dist[v.kotaTujuan]){
                        dist[v.kotaTujuan] = newDistance;
                        // System.out.println(newDistance + " vs " + dist[v.kotaTujuan] );
                    }

                    pq.add(new Edge(v.kotaTujuan, dist[v.kotaTujuan]));
                }
                else if(settled.contains(v.kotaTujuan)){
                    edgeDistance = v.weight;
                    newDistance = dist[u] + edgeDistance;
                    // System.out.println(newDistance);
                    if(newDistance < dist[v.kotaTujuan]){
                        dist[v.kotaTujuan] = newDistance;
                        // System.out.println(newDistance + " vs " + dist[v.kotaTujuan] );
                        pq.add(new Edge(v.kotaTujuan, dist[v.kotaTujuan]));
                    }
                }
            }
        }
    }

    public void clearAll(){
        for(int i = 1; i<adjList.length; i++){
            adjList[i].reset();
        }
    }

    static Vertex[] getMapVertex(){
        return adjList;
    }

}

class Vertex{
    int nama;       // nama vertex
    List<Edge> koneksi; // adjacency matrix
    int dist;    // cost
    Vertex prev;    // prev vertex
    int scratch;    // 
    int count;
    ArrayList<Integer> visited;

    Vertex(int nama){
        this.nama = nama;
        koneksi = new LinkedList<Edge>();
        reset();
    }
    public void reset(){
        dist=Integer.MAX_VALUE;
        count = Integer.MAX_VALUE;
        prev = null;
        scratch = 0;
    }

    public void addEdge(Edge edgenya){
        koneksi.add(edgenya);
    }

}

class Edge implements Comparable<Edge>{
    int kotaAsal;
    int weight;
    int kotaTujuan;
    int cost;
    Vertex destination;

    Edge(int kotaAsal, int weight, int kotaTujuan, Vertex dest){
        this.kotaAsal = kotaAsal;
        this.weight = weight;
        this.kotaTujuan = kotaTujuan;
    }

    Edge(int kotaAsal, int weight){
        this.kotaAsal = kotaAsal;
        this.weight = weight; 
    }

    public int compareTo(Edge rhs){
        if (this.cost < rhs.cost) 
			return -1; 
		if (this.cost > rhs.cost) 
			return 1; 
		return 0; 
    }

    public String toString() {
        return "" + this.kotaAsal + " ke " + this.kotaTujuan + " weight" + this.weight;
    }
}

public class DonatBisnis{
    static InputReader in = new InputReader(System.in);
    static PrintWriter out = new PrintWriter(System.out);

    // CORE PROGRAM START HERE
    public static void main(String[] args){
        int jmlVertex = in.nextInt();
        int jmlEdge = in.nextInt();
        int jmlPerintah = in.nextInt();

        Graph graf = new Graph(jmlVertex);

        for(int i = 0; i < jmlEdge; i++){
            /// Kota 1, Kota 2, weight Jalan
            graf.addEdge(in.nextInt(), in.nextInt(), in.nextInt());
        }
        // graf.printGraph(out);

        for(int i = 0; i<jmlPerintah; i++){
            String perintah = in.next();
            switch (perintah) {
                case "OS":
                    ArrayList<Integer> arrTokoBuka = in.getAllToken();
                    // out.print("---OS---\n");
                    out.println(graf.openStore(arrTokoBuka));
                    break;
                
                case "CCWGD":
                    // out.print("---CCWGD---\n");
                    out.println(graf.getCCWGD(in.nextInt(),in.nextInt()));
                    break;
                
                case "LAOR":
                    // out.print("---LAOR---\n");
                    out.println(graf.laorBFS(in.nextInt(), in.nextInt()));
                    // System.out.println("Selesai");
                    break;
                
                case "LAORC":
                    // out.print("---LAORC---\n");
                    out.println(graf.laorcBFS(in.nextInt(), in.nextInt()));
                    // in.nextInt();
                    // in.nextInt();
                    // out.println("-----------");
                    break;
                
                case "SR":
                    // out.print("---SR---\n");
                    out.println(graf.findSR(in.nextInt(), in.nextInt()));
                    break;         
                    
                case "SM":
                    // out.print("---SM---\n");
                    out.println(graf.findSM(in.nextInt(), in.nextInt()));
                    break;           
            }
        }
        out.close();
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public ArrayList<Integer> getAllToken(){
            ArrayList<Integer> number = new ArrayList<Integer>();
            while(tokenizer.hasMoreTokens()){
                number.add(Integer.parseInt(tokenizer.nextToken()));
            }
            return number;
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }
} 