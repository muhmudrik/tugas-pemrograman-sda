import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.StringTokenizer;

// alamat : /mnt/d/Pendidikan/Kuliah/Code/Kodingan/SDA/TP/2-Susun_Donat

public class RakDonat {
    public static LinkedList<LinkedList<Object>> rak = new LinkedList<LinkedList<Object>>();
    private static InputReader in;
    private static PrintWriter out;

    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        olahInput();
        operasi();

        hasil();
        out.close();
    }

    public static void olahInput() throws IOException {
        int barisDonat = in.nextInt();
        for(int i = 0; i < barisDonat; i++ ){
            int banyakDonat = in.nextInt();
            LinkedList<Object> baris = new LinkedList<Object>();
            for(int j = 0; j < banyakDonat; j++ ){
                int chip = in.nextInt();
                baris.head.next= new LinkedList.ListNode(chip);
            }
        }
    }

    public static void operasi() {
        int banyakOperasi = in.nextInt();
        for(int i = 0; i < banyakOperasi; i++){
            String kodeOperasi = in.next();
            // System.out.println(kodeOperasi);
                if(kodeOperasi.equals("IN_FRONT")){
                    inFront();
                }
                else if(kodeOperasi.equals("OUT_FRONT")){
                    outFront();
                }
                else if(kodeOperasi.equals("IN_BACK")){
                    inBack();
                }
                else if(kodeOperasi.equals("OUT_BACK")){
                    outBack();
                }
                else if(kodeOperasi.equals("MOVE_FRONT")){
                    moveFront();
                }
                else if(kodeOperasi.equals("MOVE_BACK")){
                    moveBack();
                }
                else if(kodeOperasi.equals("NEW")){
                    baru();
                }
                // System.out.println("berofe" + rak);
                urut();
                
                // System.out.println("after" + rak);
            
        }
    }

    public static void urut() {
       
    }

    public static boolean bandingIsi() {
        return false;
    }

    public static void hasil() throws IOException{
                
    }

    public static void inFront(){
        int chip = in.nextInt();
        int baris = in.nextInt()-1;
        
    }

    public static void outFront() {
        int baris = in.nextInt() -1;
        
    }

    public static void inBack() {
        int chip = in.nextInt();
        int baris = in.nextInt()-1;
        
    }

    public static void outBack() {
        int baris = in.nextInt()-1;
        
    }

    public static void moveFront() {
        int barisAsal = in.nextInt()-1;
        int barisTujuan = in.nextInt()-1;
        
    }

    public static void moveBack() {
        int barisAsal = in.nextInt()-1;
        int barisTujuan = in.nextInt()-1;
        
    }

    public static void baru() {
        
    }

    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public String nextLine() throws IOException{
            return reader.readLine();
        }

    }
}

class LinkedList <Type> {
    ListNode head;
    ListNode tail;

    LinkedList(){
        head= new ListNode();
        tail=new ListNode();
        head.next=tail;
        tail.prev=head;
    }

    public void setHead(ListNode a){
        head=a;
    }

    public void setTail(ListNode b){
        head=b;
    }

    public class ListNode {
        Object data;// data yang disimpan
        ListNode next;
        ListNode prev;

        // constructors
        ListNode(){
            data=null;
            next=null;
            prev=null;
        }
        ListNode (Object theElement) {
            this (theElement, null, null);
        }
        ListNode (Object theElement, ListNode n) {
            data = theElement;
            next = n;
        }
        ListNode (Object theElement, ListNode n, ListNode p) {
            data = theElement;
            next = n;
            prev = p;
        }

        public void setNext(ListNode node){
            next=node;
        }

        public void setPrev(ListNode node){
            prev=node;
        }
    }
}