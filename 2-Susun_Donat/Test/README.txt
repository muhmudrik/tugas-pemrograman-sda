cara jalanin testcase
Asumsi: nama file javamu TP2.java.

1. extract semua testcase ke folder yang berisi kode javamu.

2. Buka cmd/terminal, navigasi ke folder yang berisi kode javamu.

3. ketik "javac TP2.java"

4. ketik "java TP2 < TP2_1.in > TP2_my.out". Ini akan menjalankan program. Amati waktu eksekusinya ya! kalau udah lebih dari 3 detik berarti programnya kurang cepat :(

5. ketik "diff TP2_1.out TP2_my.out"

6. kalau misal gaada perbedaan, harusnya perintah terakhir ga nyetak apa-apa. Kalau misal ada yang beda, artinya output programmu beda dengan output programku (galang). Antara programku atau programmu yang salah :"(

7. Bisa dijalankan juga untuk pasangan input/output kedua (TP2_2.in/TP2_2.out)
