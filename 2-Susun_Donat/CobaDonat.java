import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;


public class CobaDonat {
    public static LinkedList<Integer> berubah = null;
    public static int indexBerubah;
    private static ArrayList<LinkedList<Integer>> rak = new ArrayList<>();
    private static InputReader in;
    private static PrintWriter out;

    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        olahInput();

        operasi();
        // System.out.println("PRINTING");
        hasil();
        out.close();
    }

    public static void olahInput() throws IOException {
        int barisDonat = in.nextInt();
        for(int i = 0; i < barisDonat; i++ ){
            int banyakDonat = in.nextInt();
            LinkedList<Integer> donat = new LinkedList<>();
            for(int j = 0; j < banyakDonat; j++ ){
                Integer chip = in.nextInt();
                donat.add(chip);
            }
            rak.add(donat);
        }
    }

    public static void operasi() {
        int banyakOperasi = in.nextInt();
        for(int i = 0; i < banyakOperasi; i++){
            String kodeOperasi = in.next();
            // System.out.println(kodeOperasi);
            // try{
                // System.out.println("awal"+rak);
                if(kodeOperasi.equals("IN_FRONT")){
                    inFront();
                }
                else if(kodeOperasi.equals("OUT_FRONT")){
                    outFront();
                }
                else if(kodeOperasi.equals("IN_BACK")){
                    inBack();
                }
                else if(kodeOperasi.equals("OUT_BACK")){
                    outBack();
                }
                else if(kodeOperasi.equals("MOVE_FRONT")){
                    moveFront();
                }
                else if(kodeOperasi.equals("MOVE_BACK")){
                    moveBack();
                }
                else if(kodeOperasi.equals("NEW")){
                    baru();
                }
                // System.out.println("berofe" + rak);
                urut();
                // System.out.println(rak.size());
                // System.out.println(rak.get(rak.size()-1));
                berubah=null;
                indexBerubah=-1;
        }
    }

    public static void urut() {
        if(rak.size()>1){
            // rak.remove(berubah);
            // rak.add(berubah);
            if(berubah!=null && indexBerubah!=-1){
                for (int ii = indexBerubah; ii < rak.size(); ii++) {
                    LinkedList<Integer> temp = rak.get(ii);
                    int jj = ii;
                    while (( jj > 0) && (bandingIsi(temp,rak.get(jj -1)))) {
                        a[jj] = a[jj -1];
                        jj--;
                    }
                    a[jj] = temp;
                }
            }
        }  
    }
    

    public static boolean bandingIsi(LinkedList<Integer> a, LinkedList<Integer> b) {
        try {
            for(int i = 0; i<a.size(); i++){
                if(a.get(i).compareTo(b.get(i))>0){
                    return true;
                }
                else if(a.get(i).compareTo(b.get(i))<0){
                    return false;
                }
            }
        } catch (IndexOutOfBoundsException e) {
            return true;
        }
        return false;
        
    }

    public static void hasil() throws IOException{
        int ukurak = rak.size();
        int ukurbaris;
        for (int i = 0; i < ukurak; i++) {
            ukurbaris = rak.get(i).size();
            for(int j = 0; j<ukurbaris; j++){
                out.print(rak.get(i).get(j));
                if(j<ukurbaris-1){
                    out.print(" ");
                }
            }
            out.println();
        }
        
    }

    public static void inFront(){
        int chip = in.nextInt();
        int baris = in.nextInt()-1;
        rak.get(baris).addFirst(chip);
        berubah=rak.get(baris);
        indexBerubah=baris;
    }

    public static void outFront() {
        int baris = in.nextInt() -1;
        if(rak.get(baris).size()>1){
            rak.get(baris).removeFirst();
            berubah=rak.get(baris);
            indexBerubah=baris;
        }
        else{
            rak.remove(baris);
        }
        
        // System.out.println(rak.get(baris));
    }

    public static void inBack() {
        int chip = in.nextInt();
        int baris = in.nextInt()-1;
        rak.get(baris).addLast(chip);
        berubah = rak.get(baris);
        indexBerubah=baris;
    }

    public static void outBack() {
        int baris = in.nextInt()-1;
        if(rak.get(baris).size()>1){
            rak.get(baris).removeLast();
            berubah = rak.get(baris);
            indexBerubah=baris;
        }
        else{
            rak.remove(baris);
        }
    }

    public static void moveFront() {
        int barisAsal = in.nextInt()-1;
        int barisTujuan = in.nextInt()-1;
        LinkedList<Integer> temp = rak.get(barisAsal);
        for(int i = temp.size()-1; i>=0; i--){
            rak.get(barisTujuan).addFirst(temp.get(i));
            // rak.get(barisTujuan).offerFirst(temp.get(i));
        }
        berubah=rak.get(barisTujuan);
        indexBerubah=barisTujuan;
        rak.remove(barisAsal);  
    }

    public static void moveBack() {
        int barisAsal = in.nextInt()-1;
        int barisTujuan = in.nextInt()-1;
        LinkedList<Integer> temp = rak.get(barisAsal);
        for(int  i = 0; i<temp.size(); i++){
            rak.get(barisTujuan).addLast(temp.get(i));
        }
        // rak.get(barisTujuan).addAll(temp);
        berubah=rak.get(barisTujuan);
        indexBerubah=barisTujuan;
        rak.remove(barisAsal);
    }

    public static void baru() {
        LinkedList<Integer> temp = new LinkedList<Integer>();
        temp.add(in.nextInt());
        rak.add(temp);
        int ukurat= rak.size()-1;
        berubah=rak.get(ukurat);
        indexBerubah=ukurat;
    }

    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public String nextLine() throws IOException{
            return reader.readLine();
        }

    }
}

/*Old Sorting method
public static void urut() {
        if(rak.size()>1){
            rak.remove(berubah);
            if(berubah!=null){
                // boolean ketemuTempat = false;
                for(int i = 0; i < rak.size(); i++){
                    // System.out.println("banding" + rak.get(i) + berubah);
                    if(bandingIsi(rak.get(i), berubah)){
                        // System.out.println("rak i menang");
                        rak.add(i, berubah);
                        break;
                    }
                    else if(i==rak.size()-1){
                        rak.add(berubah);
                        break;
                    }
                }
            }
        }
    }
}
*/